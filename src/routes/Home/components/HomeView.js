import React from 'react'
import DuckImage from '../assets/Duck.jpg'
import './HomeView.scss'
import axios from 'axios'

class HomeView extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			data: []
		}
	}

	_loadAPI(){
		var self = this
		axios({
		  method:'get',
		  url:'https://jsonplaceholder.typicode.com/posts',
		  responseType:'json'
		})
		.then(function (response) {
			if(response && response.status == 200 && response.data){
				self.setState({data: response.data})
			}
		})
		.catch(function (error) {
			console.log(error)
		});
	}

	render(){
		return (
			<div>
				<h4>Welcome!</h4>
				<img alt='This is a duck, because Redux!' className='duck' src={DuckImage} />
				<button onClick={this._loadAPI.bind(this)}>Load API Data</button>
				
				<table style={{border:"1px solid #eaeaea"}}>
					<thead>
						<tr>
							<th style={{minWidth:200}}>Title</th>
							<th>Body</th>
						</tr>
					</thead>
					<tbody>
						{
							this.state.data && this.state.data.map(function(o,i){
								return (
									<tr key={i}>
										<td>{o.title}</td>
										<td>{o.body}</td>
									</tr>
								)
							})
						}
					</tbody>
				</table>
			</div>
		)
	}
}

// export const HomeView = () => (
//   <div>
//     <h4>Welcome!</h4>
//     <img alt='This is a duck, because Redux!' className='duck' src={DuckImage} />
//   </div>
// )

export default HomeView
